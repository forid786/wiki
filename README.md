# Feral Hosting's Wiki

The `src/` directory contains the pages for Feral Hosting's Wiki at https://www.feralhosting.com/wiki

Each file must have a `.html` extension and is expected to contain HTML5 mark up. The Wiki page path maps directly onto the `src` folder. For example https://www.feralhosting.com/wiki/path/to/page maps to `src/wiki/path/to/page.html`. Paths ending in `/` are mapped to `/index.html`.

More information on contributing can be found on the Wiki itself: https://www.feralhosting.com/wiki/contributing

## Contact

wiki@feralhosting.com

## Licence

All submissions and content are owned exclusively by Feral Hosting. Submitting information to the Wiki is considered acceptance of this.

Content may not be used elsewhere without explicit permission from Feral Hosting.
