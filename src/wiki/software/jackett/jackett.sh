#!/usr/bin/env bash
set -euo pipefail
#
# Copyright (c) 2017 Feral Hosting. This content may not be used elsewhere without explicit permission from Feral Hosting.
#
# This script can be used to install Jackett and its dependencies. It can also restart and uninstall it.

# Functions

jackettMenu ()
{
    echo -e "\033[36m""Jackett (without mono)""\e[0m"
    echo "1 Install Jackett"
    echo "2 Restart Jackett"
    echo "3 Uninstall Jackett"
    echo "q Quit the script"
}

portGenerator () # generates a port to use with software installs
{
    portGen=$(shuf -i 10001-32001 -n 1)
}

portCheck () # runs a check to see if the port generated can be used
{
    while [[ "$(ss -ln | grep ':'"$portGen"'' | grep -c 'LISTEN')" -eq "1" ]];
    do
        portGenerator;
    done
}

proxypassGenerator ()
{
    if [[ -f ~/.apache2/pid ]]
    then
        echo "Installing proxypass for Apache2..."
        if [[ -f ~/.apache2/conf.d/$software.conf ]]
        then
            rm ~/.apache2/conf.d/$software.conf
            /usr/sbin/apache2ctl -k graceful > /dev/null 2>&1
        fi
        echo "Include /etc/apache2/mods-available/proxy.load
Include /etc/apache2/mods-available/proxy_http.load
Include /etc/apache2/mods-available/headers.load

ProxyRequests Off
ProxyPreserveHost On
ProxyVia On

ProxyPass /$software http://10.0.0.1:$portGen/${USER}/$software
ProxyPassReverse /$software http://10.0.0.1:$portGen/${USER}/$software" > ~/.apache2/conf.d/$software.conf
        /usr/sbin/apache2ctl -k graceful > /dev/null 2>&1
    elif [[ -f ~/.nginx/pid ]]
    then
        echo "Installing proxypass for Nginx..."
        if [[ -f ~/.nginx/conf.d/000-default-server.d/$software.conf ]]
        then
            rm ~/.nginx/conf.d/000-default-server.d/$software.conf
            /usr/sbin/nginx -s reload -c ~/.nginx/nginx.conf > /dev/null 2>&1
        fi
        echo "location /$software {
proxy_set_header X-Real-IP \$remote_addr;
proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
proxy_set_header Host \$http_x_host;
proxy_set_header X-NginX-Proxy true;

rewrite /(.*) /$(whoami)/\$1 break;
proxy_pass http://10.0.0.1:$portGen/;
proxy_redirect off;
}" > ~/.nginx/conf.d/000-default-server.d/$software.conf
        /usr/sbin/nginx -s reload -c ~/.nginx/nginx.conf > /dev/null 2>&1
    else
        echo "Neither Apache2 nor Nginx appear to be running correctly..."
        echo
    fi
}

while [[ 1 ]]
do
    echo
    jackettMenu
    echo
    read -ep "Enter the number of the option you want: " CHOICE
    echo
    case "$CHOICE" in
        "1") # install Jackett
            echo "Getting Jackett..."
            wget -qO ~/Jackett.tar.gz $(curl -s https://api.github.com/repos/Jackett/Jackett/releases/latest | grep 'browser_download_url' | grep 'LinuxAMDx64' | cut -d\" -f4 | tail -n 1)
            echo "Extracting and configuring Jackett..."
            tar xf ~/Jackett.tar.gz
            mkdir -p ~/tmp
            rm ~/Jackett.tar.gz
            mkdir -p ~/.config/Jackett
            portGenerator
            portCheck
            echo "{
  \"Port\": $portGen,
  \"AllowExternal\": true,
  \"APIKey\": \"\",
  \"AdminPassword\": null,
  \"InstanceId\": \"$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1)\",
  \"BlackholeDir\": null,
  \"UpdateDisabled\": false,
  \"UpdatePrerelease\": false,
  \"BasePathOverride\": \"/$(whoami)/jackett\",
  \"OmdbApiKey\": null
}" > ~/.config/Jackett/ServerConfig.json
            echo "Adding cron task to autostart on server reboot..."
            if [[ "$(crontab -l 2> /dev/null | grep -oc "^\@reboot screen -dmS jackett /bin/bash -c 'export TMPDIR=~/tmp; ~/Jackett/jackett'$")" == "0" ]]
            then
                tmpcron="$(mktemp)"
                crontab -l 2>/dev/null > "$tmpcron" || true
                echo "@reboot screen -dmS jackett /bin/bash -c 'export TMPDIR=~/tmp; ~/Jackett/jackett'" >> "$tmpcron"
                crontab "$tmpcron"
                rm "$tmpcron"
            else
                echo "This cron job already exists in the crontab"
                echo
            fi
            echo "Setting up the ProxyPass..."
            software=jackett
            proxypassGenerator
            echo "Starting up Jackett..."
            screen -dmS jackett /bin/bash -c 'export TMPDIR=~/tmp; ~/Jackett/jackett'
            echo "You have now installed $(~/Jackett/jackett --version | tail -n 1)"
            echo "Access Jackett at https://$(hostname -f)/$(whoami)/$software/"
            echo
            ;;
        "2") # restart Jackett
            echo "Restarting Jackett..."
            pkill -fxu $(whoami) 'jackett' || true
            screen -dmS jackett /bin/bash -c 'export TMPDIR=~/tmp; ~/Jackett/jackett'
            ;;
        "3") # uninstall jackett
            echo -e "Uninstalling Jackett will" "\033[31m""remove the software and the config files""\e[0m"
            read -ep "Are you sure you want to uninstall? [y] yes or [n] no: " CONFIRM
            if [[ $CONFIRM =~ ^[Yy]$ ]]
            then
                pkill -fxu $(whoami) 'jacket' || true
                sleep 2 # kill jackett and wait
                rm -rf ~/Jackett ~/.config/Jackett # remove directories
                # Remove crontab
                crontab -u $(whoami) -l | grep -v "@reboot screen -dmS jackett /bin/bash -c 'export TMPDIR=~/tmp; ~/bin/mono --debug ~/Jackett/JackettConsole.exe'" | crontab -u $(whoami) -
                crontab -u $(whoami) -l | grep -v "@reboot screen -dmS jackett /bin/bash -c 'export TMPDIR=~/tmp; ~/Jackett/jackett'" | crontab -u $(whoami) -
                echo "Jackett has been removed."
            else
                echo "Taking no action..."
                echo
            fi
            ;;
        "q") # quit the script entirely
            exit
            ;;
    esac
done
