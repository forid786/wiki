<h1>Plex</h1>
<p><a href="https://support.plex.tv/articles/200288286-what-is-plex/">Plex</a> lets you stream your media anywhere, on any device.</p>
<p>We provide support for running a full instance (as opposed to shared) of Plex Media Server on a slot via a Docker container. The server has access to all of your media and enables streaming to other devices.</p>

<details id="toc" open>
    <summary>Table of contents</summary>
    <nav>
        <ol>
            <li>
                <ol>
                    <li><a href="#install-claim">Installation</a></li>
                    <li><a href="#install-data">Configuring your library (use /data)</a></li>
                    <li><a href="#install-port">Enable remote access</a></li>
                </ol>
            </li>
            <li><a href="#start-stop-restart">Starting, stopping, updating and restarting</a></li>
            <li><a href="#usage">Using Plex</a></li>
            <li><a href="#userns">Moving from <kbd>userns</kbd> to a Docker container</a></li>
            <li><a href="#troubleshooting">Troubleshooting</a></li>
            <li><a href="#uninstallation">Uninstallation</a></li>
            <li><a href="#external-links">External links</a></li>
        </ol>
    </nav>
</details> 

<h2 id="install-claim">Installation</h3>
<p>To set up and install Plex Media Server on your slot, please do the following:</p>
<ol>
    <li>Get a <a href="https://www.plex.tv/claim/">claim code</a>. Note: a claim code is only valid for 5 minutes.</li>
    <li>Copy this claim code to the file <kbd>~/private/plex.claim</kbd> or <kbd>~/private/plex.claim.txt</kbd> using your <a href="/wiki/slots/ftp">SFTP / FTP client</a>. Note: do <em>not</em> create a <kbd>~/private/plex</kbd> folder.</li>
    <li>After a minute, check whether the installation was a success with the presence of the file <kbd>install-was-a-success</kbd> in the Plex folder. If it was unsuccessful you will see a similarly named file of the form <kbd>install-<em>description-of-error</em></kbd>.</li>
</ol>
<p>Finally, visit the URL found in <kbd>~/private/plex/web.url</kbd> in your browser to use Plex. You should not put any media inside <kbd>~/private/plex</kbd> as the folder is treated as disposable.</p>

<h3 id="install-data">Configure your library</h3>
<p>In Plex you can find your slot's data under the path <kbd>/data</kbd>.</p>
<p>When you first login to Plex it will present you with a "Welcome to Plex!" screen and prompt for an initial set up giving you the chance to add a media library. Afterwards, you can <a href="https://support.plex.tv/articles/200288926-creating-libraries/">create libraries</a> through the settings menu.</p>

<h3 id="install-port">Enable remote access</h3>
<p>Some users have reported that they see unreachable errors such as: "Your server is signed in to Plex, but is not reachable from outside your network". This can also be characterised as slow streaming speeds, much slower than using SFTP / FTP. Restarting may appear to fix the issue for a short period.</p>
<p>By default Plex relies on upnp for remote access but does not appear to always be reliable. The solution is to make Plex aware of the manual port forwarding that is configured by our system.</p>
<ol>
    <li>Visit "Settings" from the link in the top-right hand side, select "Remote Access" from the menu panel that appears on the left-hand side and ensure "SHOW ADVANCED" is selected on the page.</li>
    <li>Find your forwarded port: in the URL just after "server.feralhosting.com:" should be a port number. For instance if you saw "http://server.feralhosting.com:<var>PORT</var>/web/index.html" you would use <var>PORT</var>.</li>
    <li>On the page presented ensure "Manually specify public port" is checked and the text field next to it has has your URL's port number (found in the previous step).</li>
    <li>Hit the "APPLY" button and wait a short while for Plex to reconfigure itself.</li>
    <li>This will sometimes configure the manua port forwarding but disable remote access as a whole. If this happens you will see the "ENABLE REMOTE ACCESS" button which should be pressed.</li>
</ol>
<p>Plex also have their own guide on <a href="https://support.plex.tv/articles/200289506-remote-access/">configuring remote access</a>.</p>

<h2 id="start-stop-restart">Starting, stopping, updating and restarting Plex</h2>
<aside class="alert note">This section will not work on previous <kbd>userns</kbd> installations. We recommend <a href="#userns">moving to a new installation</a>.</aside>

<p>Once installed you can use <em>control files</em> to make changes to Plex's running state. Deleting files in the folder <kbd>~/private/plex/control-files</kbd> will result in their named action i.e., deleting "restart" will trigger a restart. Actions take one minute to be executed by the system.</p>

<p>If Plex is running the following <a href="https://www.feralhosting.com/wiki/slots/ssh">SSH command</a> will restart Plex:</p>
<pre><code>rm ~/private/plex/control-files/restart</code></pre>

<p>Use the following <a href="https://www.feralhosting.com/wiki/slots/ssh">SSH command</a> to update Plex:</p>
<pre><code>rm ~/private/plex/control-files/update</code></pre>

<p>If Plex is running the following <a href="https://www.feralhosting.com/wiki/slots/ssh">SSH command</a> will stop Plex:</p>
<pre><code>rm ~/private/plex/control-files/stop</code></pre>

<p>If Plex is stopped the following <a href="https://www.feralhosting.com/wiki/slots/ssh">SSH command</a> will start Plex:</p>
<pre><code>rm ~/private/plex/control-files/start</code></pre>

<p>Control files can also be deleted using an <a href="https://www.feralhosting.com/wiki/slots/ftp">SFTP or FTP client</a> meaning SSH is not necessary.</p>

<h2 id="usage">Using Plex</h2>
<p>For help using the software itself you should consult <a href="https://www.plex.tv/support/">Plex's own support page</a> first of all as it's likely that your question is covered there.</p>

<p>In Plex, your slot data can be found under the path <code>/data</code></p>

<h2 id="userns">Moving from a <kbd>userns</kbd> install to a container install</h2>
<p>Previous installations ran under your slot's <kbd>userns</kbd> an environment that uses namespaces to separate users. New installations (using claim codes) run using Plex's own Docker image. The Docker image can be considered as close to a standard environment as possible which will help Plex and Feral to both diagnose issues that may arise.</p>

<p>The <kbd>userns</kbd> installation will continue to work and will be restarted if <kbd>~/private/plex/usr</kbd> exists. If you run into problems with the <kbd>userns</kbd> set up you should uninstall and then <a href="#installation">perform a new style installation</a>. We recommend moving to this new set up as soon as possible even if you are not having experiencing issues.</p>

<p>To uninstall Plex and stop using the <kbd>userns</kbd>, run the following <a href="https://www.feralhosting.com/wiki/slots/ssh">SSH command</a>:</p>
<pre><code>rm -r ~/private/plex</code></pre>

<h2 id="troubleshooting">Troubleshooting</h2>
<dl>
    <dt>You see the error "Your server is signed in to Plex, but is not reachable from outside your network".</dt>
    <dd>Ensure that <a href="#install-port">manual port forwarding</a> is set up.</dd>
    
    <dt>You see the warning "Unable to connect to "plex.user.server" securely".</dt>
    <dd>Ensure that <a href="#install-port">manual port forwarding</a> is set up.</dd>

    <dt>Plex is slower than SFTP / FTP, buffers a lot and is generally slow.</dt>
    <dd>Ensure that <a href="#install-port">manual port forwarding</a> is set up.</dd>

    <dt>Manual port forwarding is set up but Plex is still slow.</dt>
    <dd>Use the <a href="https://www.feralhosting.com/wiki/slots/more-speed#rerouting">reroute page</a> to try and improve speeds.</dd>
</dl>

<h2 id="uninstallation">Uninstalling</h2>
<p>The <a href="https://www.feralhosting.com/wiki/slots/ssh">SSH command</a> below will completely remove Plex binaries and its configuration.</p>
<pre><code>rm -r ~/private/plex</code></pre>

<h2 id="external-links">Other links</h2>
<ul>
    <li><a href="/wiki/software/plex/plex-webtools">Installing the WebTools plugin</a></li>
    <li><a href="https://www.plex.tv/">Plex homepage</a>
        <ul>
            <li><a href="https://www.plex.tv/support/">Support documentation</a></li>
            <li><a href="https://forums.plex.tv/">Community</a></li>
        </ul>
    </li>
</ul>