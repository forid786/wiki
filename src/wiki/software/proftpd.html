<h1>ProFTPd</h1>
<aside class="alert note">Feral cannot provide support for custom ProFTPd daemons beyond the contents of this guide.</aside>

<p>This article will show you how to install a custom ProFTPd daemon. While each user has FTP and SFTP access to their slots, a custom ProFTPd daemon will allow you to set up other users and jail them to directories on your slot, or connect via FTPS.</p>

<p>You'll need to execute some commands via SSH to use this software. There is a separate guide on how to <a href="/wiki/slots/ssh">connect to your slot via SSH</a>. Commands are kept as simple as possible and in most cases will simply need to be copied and pasted into the terminal window (then executed by pressing the <kbd>Enter</kbd> key).</p>

<details id="toc" open>
    <summary>Table of contents</summary>
    <nav>
        <ol>
            <li><a href="#installation">Installation</a></li>
            <li><a href="#basic-setups">Basic ProFTPd Configuration</a></li>
            <li><a href="#start">Starting, stopping and restarting</a>
                <ul>
                    <li><a href="#auto-restart">Automatically restarting ProFTPd if it is not running</a></li>
                </ul>
            </li>
            <li><a href="#usage">Using ProFTPd</a></li>
            <li><a href="#configure">Further Configuration - Adding Jails and Custom Users</a></li>
            <li><a href="#uninstallation">Uninstallation</a></li>
            <li><a href="#external-links">External links</a></li>
        </ol>
    </nav>
</details> 

<h2 id="installation">Installation</h2>
<p>The following steps will get ProFTPd's source and build it on your slot. You run the commands below by logging in via SSH, then copying and pasting the following:</p>
<pre><kbd>wget -qO ~/proftpd.tar.gz ftp://ftp.proftpd.org/distrib/source/proftpd-1.3.6.tar.gz
tar xf ~/proftpd.tar.gz -C ~/
cd proftpd-*
install_user=$(whoami) install_group=$(whoami) ./configure --prefix=$HOME/proftpd --enable-openssl --enable-dso --enable-nls --enable-ctrls --with-shared=mod_ratio:mod_readme:mod_sftp:mod_tls:mod_ban:mod_shaper
make &amp;&amp; make install
mkdir -p ~/proftpd/etc/sftp/authorized_keys ~/proftpd/etc/keys ~/proftpd/ssl
cd &amp;&amp; rm -rf proftpd-* proftpd.tar.gz</kbd></pre>

<h2 id="basic-setup">Basic ProFTPd Configuration</h2>

<p>Three things must be completed:</p>
<ol>
    <li>Grab the configs and tweak the default settings</li>
    <li>Generate keys and certificates for SFTP and FTPS</li>
    <li>Create the main user and group</li>
</ol>
<p>Each of the three things above can be done by copying and pasting the relevant commands found below.</p>

<p>Grab three default config files and tweak them for your main Feral user:</p>
<pre><kbd>wget -qO ~/proftpd/etc/proftpd.conf https://bitbucket.org/feralio/wiki/raw/HEAD/src/wiki/software/proftpd/proftpd.conf
wget -qO ~/proftpd/etc/sftp.conf https://bitbucket.org/feralio/wiki/raw/HEAD/src/wiki/software/proftpd/sftp.conf
wget -qO ~/proftpd/etc/ftps.conf https://bitbucket.org/feralio/wiki/raw/HEAD/src/wiki/software/proftpd/ftps.conf
sed -i 's|/media/DiskID/defaultUser|'$HOME'|g' ~/proftpd/etc/proftpd.conf
sed -i 's|User defaultUser|User '$(whoami)'|g' ~/proftpd/etc/proftpd.conf
sed -i 's|Group defaultGroup|Group '$(whoami)'|g' ~/proftpd/etc/proftpd.conf
sed -i 's|AllowUser defaultUser|AllowUser '$(whoami)'|g' ~/proftpd/etc/proftpd.conf
sed -i 's|/media/DiskID/defaultUser|'$HOME'|g' ~/proftpd/etc/sftp.conf
sed -i 's|Port 23001|Port '$(shuf -i 10001-49999 -n 1)'|g' ~/proftpd/etc/sftp.conf
sed -i 's|/media/DiskID/defaultUser|'$HOME'|g' ~/proftpd/etc/ftps.conf
sed -i 's|Port 23002|Port '$(shuf -i 10001-49999 -n 1)'|g' ~/proftpd/etc/ftps.conf</kbd></pre>

<p>The next thing to do is generate some keys and certificates for our SFTP and FTPS setups. Copy and paste the following commands to do this:</p>
<pre><kbd>ssh-keygen -qt rsa -N '' -f ~/proftpd/etc/keys/sftp_rsa
ssh-keygen -qt dsa -N '' -f ~/proftpd/etc/keys/sftp_dsa
openssl req -new -x509 -nodes -days 365 -subj '/C=GB/ST=none/L=none/CN=none' -newkey rsa:2048 -keyout ~/proftpd/ssl/proftpd.key.pem -out ~/proftpd/ssl/proftpd.cert.pem</kbd></pre>

<p>Finally we need to create our main user and group. Copy and paste and enter a password when prompted:</p>
<pre><kbd>~/proftpd/bin/ftpasswd --group --name $(whoami) --file ~/proftpd/etc/ftpd.group --gid $(id -g $(whoami)) --member $(whoami)
~/proftpd/bin/ftpasswd --passwd --name $(whoami) --file ~/proftpd/etc/ftpd.passwd --uid $(id -u $(whoami)) --gid $(id -g $(whoami)) --home $HOME/ --shell /bin/false</kbd></pre>
<p>Note that, as with any use of passwd, your input will not be interactive. There will not be <samp>*****</samp> displayed as you type.</p>

<h2 id="start">Starting, stopping and restarting</h2>
<p>This section covers the ProFTPd process - starting it, stopping it and restarting it. It also covers checking if the process is running, in case that becomes necessary.</p>

<h3>SFTP</h3>
<dl>
    <dt>start</dt>
    <dd><kbd>~/proftpd/sbin/proftpd -c ~/proftpd/etc/sftp.conf</kbd></dd>
    <dt>check running</dt>
    <dd><kbd>cat ~/proftpd/sftp.pid</kbd></dd>
    <dt>stop</dt>
    <dd><kbd>cat ~/proftpd/sftp.pid | xargs kill</kbd></dd>
    <dt>restart</dt>
    <dd><kbd>cat ~/proftpd/sftp.pid | xargs kill &amp;&amp; sleep 15 &amp;&amp; ~/proftpd/sbin/proftpd -c ~/proftpd/etc/sftp.conf</kbd></dd>
    <dt>kill (force stop)</dt>
    <dd><kbd>cat ~/proftpd/sftp.pid | xargs kill -9 &amp;&amp; rm -rf ~/proftpd/sftp.pid</kbd></dd>
</dl>

<h3>FTPS</h3>
<dl>
    <dt>start</dt>
    <dd><kbd>~/proftpd/sbin/proftpd -c ~/proftpd/etc/ftps.conf</kbd></dd>
    <dt>check running</dt>
    <dd><kbd>cat ~/proftpd/ftps.pid</kbd></dd>
    <dt>stop</dt>
    <dd><kbd>cat ~/proftpd/ftps.pid | xargs kill</kbd></dd>
    <dt>restart</dt>
    <dd><kbd>cat ~/proftpd/ftps.pid | xargs kill &amp;&amp; sleep 15 &amp;&amp; ~/proftpd/sbin/proftpd -c ~/proftpd/etc/ftps.conf</kbd></dd>
    <dt>kill (force stop)</dt>
    <dd><kbd>cat ~/proftpd/ftps.pid | xargs kill -9 &amp;&amp; rm -rf ~/proftpd/ftps.pid</kbd></dd>
</dl>

<p>The check commands work as follows: if the process is running a list of relevant process ID numbers will be listed; if <samp>No such file or directory</samp> is returned the process is not running.</p>
<p>Please note that the bash script at the top of the page can also be used for restarting ProFTPd.</p>

<h3 id="auto-restart">Automatically restarting ProFTPd if it is not running</h3>
<p>Cron jobs can be used to check if ProFTPd is running and start it up if it is not. There is a separate page on <a href="/wiki/slots/cron">configuring cron jobs</a>.</p>

<h2 id="usage">Using ProFTPd</h2>
<p>Connecting to the custom daemon is almost the same as connecting to the Feral ProFTPd daemon. The main difference is that you need to supply a port - the custom port your processes are running on.</p>
<p>To get the port for your custom SFTP process, run the following command:</p>
<p><kbd>sed -nr 's/^Port (.*)/\1/p' ~/proftpd/etc/sftp.conf</kbd></p>
<p>To get the port for your custom FTPS process, run the following command:</p>
<p><kbd>sed -nr 's/^Port (.*)/\1/p' ~/proftpd/etc/ftps.conf</kbd></p>

<h2 id="configure">Further Configuration - Adding Jails and Custom Users</h2>
<p>Following this guide will leave you with a custom daemon that your main user can access via either SFTP or FTPS. While the addition of FTPS connectivity adds something over and above the default Feral-managed ProFTPd daemon, this is not the limit to what can be done. You can add further users and jails (the directories which a user can access) to extend the functionality.</p>
<p>A script has been created to take you through the creation of jails and users. Call it with the following command:</p>
<p><kbd>wget -qO ~/proftpd.adduser.sh https://bitbucket.org/feralio/wiki/raw/HEAD/src/wiki/software/proftpd/proftpd.adduser.sh &amp;&amp; bash ~/proftpd.adduser.sh</kbd></p>

<aside class="alert note">If ProFTPd is running when you create a jail/user you'll need to restart it for changes to take affect.</aside>

<p>Report any issues with the script in a ticket. Ensure you let staff know exactly what you entered and what the script returned so they can try to help you.</p>

<h2 id="uninstallation">Uninstallation</h2>
<aside class="alert note">The commands below will completely remove the software and its config files - back up important data first!</aside>

<pre><code>cat ~/proftpd/sftp.pid | xargs kill
cat ~/proftpd/ftps.pid | xargs kill
rm -rf ~/proftpd/</code></pre>

<h2 id="external-links">External links</h2>
<ul>
    <li><a href="http://www.proftpd.org/docs/">ProFTPd documentation</a></li>
</ul>