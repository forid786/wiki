<h1>Proxypassing</h1>
<p>If you have installed software like <a href="../software/sickrage">SickRage</a> or <a href="../software/sabnzbd">SABnzbd</a> you'll by default get a URL featuring a port number and be unable to validly use the web UI via HTTPS. Using proxypass you'll be able neaten up the URL but perhaps more importantly get covered by the server's HTTPS certificate.</p>
<p>You'll need to execute some commands via SSH to configure your proxypass. There is a separate guide on how to <a href="../slots/ssh">connect to your slot via SSH</a>.</p>

<details id="toc" open>
    <summary>Table of contents</summary>
    <nav>
        <ol>
            <li><a href="#apache">Apache</a>
                <ul>
                    <li><a href="#apache-example">Example with Apache</a></li>
                </ul>
            </li>
            <li><a href="#nginx">Nginx</a>
                <ul>
                    <li><a href="#nginx-example">Example with Nginx</a></li>
                </ul>
            </li>
            <li><a href="#troubleshooting">Proxypass issues</a></li>
        </ol>
    </nav>
</details> 

<h2 id="apache">Apache</h2>
<p>First, let's open up a blank config file by copying and pasting the command below (where <var>software</var> is replaced by the name of the software):</p>
<p><kbd>nano ~/.apache2/conf.d/<var>software</var>.conf</kbd></p>
<p>The name of the file doesn't actually matter (though needs to end in <samp>.conf</samp>), but it's obviously better to have it clear for future reference.</p>
<p>The blank proxypass config is below:</p>
<pre><kbd>Include /etc/apache2/mods-available/proxy.load
Include /etc/apache2/mods-available/proxy_http.load
Include /etc/apache2/mods-available/headers.load

ProxyRequests Off
ProxyPreserveHost On
ProxyVia On

ProxyPass /<var>software</var> http://10.0.0.1:<var>port</var>/${USER}/<var>software</var>
ProxyPassReverse /<var>software</var> http://10.0.0.1:<var>port</var>/${USER}/<var>software</var></kbd></pre>

<p>In the above example, replace <var>software</var> with the name of the software - this will form part of our new URL. You should also replace <var>port</var> with the port number the software is running on. If this changes, the proxypass config will also need to be changed. Please note that you do not need to replace <samp>${USER}</samp> in the above.</p>
<p>The result will be software that can be accessed via <samp>https://<var>server</var>.feralhosting.com/<var>username</var>/<var>software</var></samp>. The section below has a working example which might be helpful to you.</p>

<h3 id="apache-example">Example with Apache</h3>
<p>In this example, we've installed <a href="../software/sickrage">SickRage</a> and can access it just fine via <samp>http://<var>server</var>.feralhosting.com:11702/<var>username</var>/sickrage</samp>. We want to use HTTPS instead, but cannot. We need proxypass! First, we open the blank config file with:</p>
<p><kbd>nano ~/.apache2/conf.d/sickrage.conf</kbd></p>
<p>We make the changes described above and our config looks like this:</p>
<pre><kbd>Include /etc/apache2/mods-available/proxy.load
Include /etc/apache2/mods-available/proxy_http.load
Include /etc/apache2/mods-available/headers.load

ProxyRequests Off
ProxyPreserveHost On
ProxyVia On

ProxyPass /sickrage http://10.0.0.1:11702/${USER}/sickrage
ProxyPassReverse /sickrage http://10.0.0.1:11702/${USER}/sickrage</kbd></pre>
<p>We then need to reload the configs to be able to use the changes:</p>
<p><kbd>/usr/sbin/apache2ctl -k graceful</kbd></p>
<p>We can now access SickRage at <samp>https://<var>server</var>.feralhosting.com/<var>username</var>/sickrage</samp>.</p>

<h2 id="nginx">Nginx</h2>
<p>If you have made the <a href="../software/nginx">switch to nginx</a> you should use this section of the guide to configure proxypass.</p>
<p>Please note that if you had previously set up proxypass for software running under Apache, you'll need to redo the configs for nginx as they have a different format.</p>
<p>First, let's open up a blank config file by copying and pasting the command below (where <var>software</var> is replaced by the name of the software):</p>
<p><kbd>nano ~/.nginx/conf.d/000-default-server.d/<var>software</var>.conf</kbd></p>
<p>The name of the file doesn't actually matter (though needs to end in <samp>.conf</samp>), but it's obviously better to have it clear for future reference.</p>
<p>The blank proxypass config is below:</p>
<pre><kbd>location /<var>software</var> {
proxy_set_header X-Real-IP $remote_addr;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_set_header Host $http_x_host;
proxy_set_header X-NginX-Proxy true;

rewrite /(.*) /<var>username</var>/$1 break;
proxy_pass http://10.0.0.1:<var>port</var>/;
proxy_redirect off;
}</kbd></pre>

<p>In the above example, replace <var>software</var> with the name of the software - this will form part of our new URL, and <var>username</var> with your username. You should also replace <var>port</var> with the port number the software is running on. If this changes, the proxypass config will also need to be changed.
<p>The result will be software that can be accessed via <samp>https://<var>server</var>.feralhosting.com/<var>username</var>/<var>software</var></samp>. The section below has a working example which might be helpful to you.</p>

<h3 id="nginx-example">Example with nginx</h3>
<p>In this example, we've installed <a href="../software/sickrage">SickRage</a> and can access it just fine via <samp>http://<var>server</var>.feralhosting.com:11702/<var>username</var>/sickrage</samp>. We want to use HTTPS instead, but cannot. We need proxypass! First, we open the blank config file with:</p>
<p><kbd>nano ~/.nginx/conf.d/000-default-server.d/sickrage.conf</kbd></p>
<p>We make the changes described above and our config looks like this:</p>
<pre><kbd>location /sickrage {
proxy_set_header X-Real-IP $remote_addr;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_set_header Host $http_x_host;
proxy_set_header X-NginX-Proxy true;

rewrite /(.*) /<var>username</var>/$1 break;
proxy_pass http://10.0.0.1:11702/;
proxy_redirect off;
}</kbd></pre>
<p>We then need to reload the configs to be able to use the changes:</p>
<p><kbd>/usr/sbin/nginx -s reload -c ~/.nginx/nginx.conf</kbd></p>
<p>We can now access SickRage at <samp>https://<var>server</var>.feralhosting.com/<var>username</var>/sickrage</samp>.</p>

<h2 id="troubleshooting">Proxypass issues</h2>
<p>The guides and examples here assume you can run your software in the format <samp>http://<var>server</var>.feralhosting.com:<var>port</var>/<var>username</var>/<var>software</var></samp>. Some software might hardcode a different format, for instance rewriting out the username. By default, SABnzbd is an example of this and needs its Python script edited before it can be proxypassed. If you're running software not even covered by a guide you should ensure it is set up to run in the correct "non-proxied" format first.</p>